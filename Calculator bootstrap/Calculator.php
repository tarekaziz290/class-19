<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Calculator</title>
  </head>
  <body>
    <!-- <h1>Calculator</h1> -->
    <div class="container my-5">
        <div class="card col-md-4  offset-md-4">
            <div class="card-body">
            <form action=" " method="POST">
                <h3>Calculator</h3>
                  <div class="mb-3">
                    <label for="num1" class="form-label">Number 1</label>
   
            <input type="text" class="form-control" name="num1" required>
                  </div>
                  <div class="mb-3">
                   <label for="op" class="form-control">Select Operator</label>  
                  <select name="op" class="form-select" >
                <option value="add">+</option>
                <option value="sub">-</option>
                <option value="mul">*</option>
                <option value="div">/</option>
                <option value="mudo">%</option>

            </select>
                  </div>
            <div class="mb-3">
            <label for="num2" class="form-label">Number 2</label>

            <input type="text" class="form-control" name="num2" required>
            </div>


            

            <button type="submit" class="btn btn-info" >Calculate</button>




        </form>

       <div class="result">
        <?php
       class Calculator {

public $num1;
public $num2;
public $op;

public function dataValue($num1, $num2, $op) {
    $this->num1 = $num1;
    $this->num2 = $num2;
    $this->op = $op;
}

public function calcMethod() {
 
 if ($_SERVER["REQUEST_METHOD"] == "POST") {
    # code...
 
    switch ($this->op) {
    case 'add':
        $result = $this->num1 + $this->num2;
        break;
    case 'sub':
        $result = $this->num1 - $this->num2;
        break;
    case 'mul':
        $result = $this->num1 * $this->num2;
        break;
    case 'div':
        $result = $this->num1 / $this->num2;
        break;
    case 'mudo':
        $result = $this->num1 % $this->num2;
        break;

    default:
        $result = "Error";
        break;
  }
  return $result;
}
}

}



$num1 = $_POST['num1']?? '';
$num2 = $_POST['num2']?? '';
$op = $_POST['op']?? '';

$calculator = new Calculator;
$calculator->dataValue($num1,$num2,$op);
echo  "<h4> Result:".$calculator->calcMethod()."</h4>";

?>
       </div>

            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
  </body>
</html>

<?php


// class Calculator {

//     public $num1;
//     public $num2;
//     public $op;

//     public function dataValue($num1, $num2, $op) {
//         $this->num1 = $num1;
//         $this->num2 = $num2;
//         $this->op = $op;
//     }

//     public function calcMethod() {
     
//      if ($_SERVER["REQUEST_METHOD"] == "POST") {
//         # code...
     
//         switch ($this->op) {
//         case 'add':
//             $result = $this->num1 + $this->num2;
//             break;
//         case 'sub':
//             $result = $this->num1 - $this->num2;
//             break;
//         case 'mul':
//             $result = $this->num1 * $this->num2;
//             break;
//         case 'div':
//             $result = $this->num1 / $this->num2;
//             break;
//         case 'mudo':
//             $result = $this->num1 % $this->num2;
//             break;

//         default:
//             $result = "Error";
//             break;
//       }
//       return $result;
//     }
// }

// }



// $num1 = $_POST['num1']?? '';
// $num2 = $_POST['num2']?? '';
// $op = $_POST['op']?? '';

// $calculator = new Calculator;
// $calculator->dataValue($num1,$num2,$op);
//  echo  $calculator->calcMethod();

//  ?>